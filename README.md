# terraform-basic-network

A [terraform](https://www.terraform.io/) module to create a basic network/Virtual Private Cloud (VPC) on AWS.  The network created spans two availability zones (AZs) in a single region and has a public and private subnet in each AZ.

To use, declare the module in your terraform files like this:

```hcl
module "network" {
  availability_zone_1 = "${var.aws_availability_zone_1}"
  availability_zone_2 = "${var.aws_availability_zone_2}"
  instance = "${var.instance}"
  project_name = "${var.project_name}"
  source = "bitbucket.org/3EEnterprises/terraform-basic-network"
}
```

The resulting network is shown in the schematic below.

You can then install a bastion host on one of the public networks with something like this:

```hcl
module "instance_public" {
  aws_key_pair_auth_id = "${aws_key_pair.auth.id}"
  aws_region = "${var.aws_region}"
  instance = "${var.instance}"
  private_key_path = "${var.private_key_path}"
  project_name = "${var.project_name}"
  public_key_path = "${var.public_key_path}"
  security_group = "${module.network.aws_security_group_ssh_only_id}"
  source = "../instance_public"
  subnet_id = "${module.network.aws_subnet_public_1_id}"
  tag_name = "public_node"
}
```

Or you can install a host on one of the private networks with something like this:

```hcl
module "instance_private" {
  aws_key_pair_auth_id = "${aws_key_pair.auth.id}"
  aws_region = "${var.aws_region}"
  bastion_host = "${module.instance_public.public_ip}"
  instance = "${var.instance}"
  private_key_path = "${var.private_key_path}"
  project_name = "${var.project_name}"
  public_key_path = "${var.public_key_path}"
  security_group = "${module.network.aws_security_group_ssh_only_id}"
  source = "../instance_private"
  subnet_id = "${module.network.aws_subnet_private_1_id}"
  tag_name = "private_node"
}
```

You can create a high availability VPC by declaring a second module in a different AWS region.

![Network](example-1.png)

Questions/comments: @ErikEvenson on twitter.
