resource "aws_route_table" "private_1" {
  #propagating_vgws = []
  vpc_id = "${aws_vpc.main.id}"

  route {
    cidr_block = "0.0.0.0/0"
    #gateway_id = ""
    #instance_id = ""
    nat_gateway_id = "${aws_nat_gateway.gw_1.id}"
    #network_interface_id = ""
    #vpc_peering_connection_id = ""
  }

  tags {
    Name = "${var.project_name} (${var.instance}) / private 1"
  }
}

resource "aws_route_table" "private_2" {
  #propagating_vgws = []
  vpc_id = "${aws_vpc.main.id}"

  route {
    cidr_block = "0.0.0.0/0"
    #gateway_id = ""
    #instance_id = ""
    nat_gateway_id = "${aws_nat_gateway.gw_2.id}"
    #network_interface_id = ""
    #vpc_peering_connection_id = ""
  }

  tags {
    Name = "${var.project_name} (${var.instance}) / private 2"
  }
}

resource "aws_eip" "nat_gw_1" {
  #vpc = true
  #instance = ""
  #network_interface = ""
}

resource "aws_eip" "nat_gw_2" {
  #vpc = true
  #instance = ""
  #network_interface = ""
}

resource "aws_nat_gateway" "gw_1" {
  allocation_id = "${aws_eip.nat_gw_1.id}"
  depends_on = ["aws_internet_gateway.gw"]
  subnet_id = "${aws_subnet.public_1.id}"
}

resource "aws_nat_gateway" "gw_2" {
  allocation_id = "${aws_eip.nat_gw_2.id}"
  depends_on = ["aws_internet_gateway.gw"]
  subnet_id = "${aws_subnet.public_2.id}"
}

resource "aws_subnet" "private_1" {
  availability_zone = "${var.availability_zone_1}"
  cidr_block = "${cidrsubnet("${var.cidr_block}", 8, 3)}"
  #map_private_ip_on_launch = ""
  vpc_id = "${aws_vpc.main.id}"

  tags {
    Name = "${var.project_name} (${var.instance}) / private 1"
  }
}

resource "aws_subnet" "private_2" {
  availability_zone = "${var.availability_zone_2}"
  cidr_block = "${cidrsubnet("${var.cidr_block}", 8, 4)}"
  #map_private_ip_on_launch = ""
  vpc_id = "${aws_vpc.main.id}"

  tags {
    Name = "${var.project_name} (${var.instance}) / private 2"
  }
}

resource "aws_route_table_association" "private_1" {
  subnet_id = "${aws_subnet.private_1.id}"
  route_table_id = "${aws_route_table.private_1.id}"
}

resource "aws_route_table_association" "private_2" {
  subnet_id = "${aws_subnet.private_2.id}"
  route_table_id = "${aws_route_table.private_2.id}"
}
