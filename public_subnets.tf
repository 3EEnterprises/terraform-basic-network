resource "aws_route_table" "public_1" {
  #propagating_vgws = []
  vpc_id = "${aws_vpc.main.id}"

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.gw.id}"
    #instance_id = ""
    #nat_gateway_id = ""
    #network_interface_id = ""
    #vpc_peering_connection_id = ""
  }

  tags {
    Name = "${var.project_name} (${var.instance}) / public 1"
  }
}

resource "aws_route_table" "public_2" {
  #propagating_vgws = []
  vpc_id = "${aws_vpc.main.id}"

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.gw.id}"
    #instance_id = ""
    #nat_gateway_id = ""
    #network_interface_id = ""
    #vpc_peering_connection_id = ""
  }

  tags {
    Name = "${var.project_name} (${var.instance}) / public 2"
  }
}

resource "aws_subnet" "public_1" {
  availability_zone = "${var.availability_zone_1}"
  cidr_block = "${cidrsubnet("${var.cidr_block}", 8, 1)}"
  #map_public_ip_on_launch = ""
  vpc_id = "${aws_vpc.main.id}"

  tags {
    Name = "${var.project_name} (${var.instance}) / public 1"
  }
}

resource "aws_subnet" "public_2" {
  availability_zone = "${var.availability_zone_2}"
  cidr_block = "${cidrsubnet("${var.cidr_block}", 8, 2)}"
  #map_public_ip_on_launch = ""
  vpc_id = "${aws_vpc.main.id}"

  tags {
    Name = "${var.project_name} (${var.instance}) / public 2"
  }
}

resource "aws_route_table_association" "public_1" {
  subnet_id = "${aws_subnet.public_1.id}"
  route_table_id = "${aws_route_table.public_1.id}"
}

resource "aws_route_table_association" "public_2" {
  subnet_id = "${aws_subnet.public_2.id}"
  route_table_id = "${aws_route_table.public_2.id}"
}
