resource "aws_vpc" "main" {
  cidr_block = "${var.cidr_block}"
  enable_classiclink = false
  enable_dns_hostnames = true
  enable_dns_support = true
  instance_tenancy = "default"

  tags {
    Name = "${var.project_name} (${var.instance}) / main"
  }
}

resource "aws_network_acl" "public" {
  egress = {
    action = "allow"
    cidr_block = "0.0.0.0/0"
    #icmp_code
    from_port = 0
    #icmp_type
    protocol = "tcp"
    rule_no = 100
    to_port = 65535
  }

  ingress = {
    action = "allow"
    cidr_block = "0.0.0.0/0"
    #icmp_code
    from_port = 22
    #icmp_type
    protocol = "tcp"
    rule_no = 100
    to_port = 22
  }

  ingress = {
    action = "allow"
    cidr_block = "0.0.0.0/0"
    #icmp_code
    from_port = 80
    #icmp_type
    protocol = "tcp"
    rule_no = 200
    to_port = 80
  }

  ingress = {
    action = "allow"
    cidr_block = "0.0.0.0/0"
    #icmp_code
    from_port = 443
    #icmp_type
    protocol = "tcp"
    rule_no = 300
    to_port = 443
  }

  ingress = {
    action = "allow"
    cidr_block = "0.0.0.0/0"
    #icmp_code
    from_port = 1024
    #icmp_type
    protocol = "tcp"
    rule_no = 400
    to_port = 65535
  }

  subnet_ids = [
    "${aws_subnet.public_1.id}",
    "${aws_subnet.public_2.id}",
  ]

  vpc_id = "${aws_vpc.main.id}"

  tags {
    Name = "${var.project_name} (${var.instance}) / public"
  }
}

resource "aws_network_acl" "private" {
  egress = {
    action = "allow"
    cidr_block = "0.0.0.0/0"
    #icmp_code
    from_port = 0
    #icmp_type
    protocol = "tcp"
    rule_no = 100
    to_port = 65535
  }

  ingress = {
    action = "allow"
    cidr_block = "10.0.0.0/16"
    #icmp_code
    from_port = 22
    #icmp_type
    protocol = "tcp"
    rule_no = 100
    to_port = 22
  }

  ingress = {
    action = "allow"
    cidr_block = "0.0.0.0/0"
    #icmp_code
    from_port = 1024
    #icmp_type
    protocol = "tcp"
    rule_no = 400
    to_port = 65535
  }

  subnet_ids = [
    "${aws_subnet.private_1.id}",
    "${aws_subnet.private_2.id}",
  ]

  vpc_id = "${aws_vpc.main.id}"

  tags {
    Name = "${var.project_name} (${var.instance}) / private"
  }
}
