output "aws_security_group_ssh_only_id" {
  value = "${aws_security_group.ssh_only.id}"
}

output "aws_security_group_http_https_ssh_id" {
  value = "${aws_security_group.http_https_ssh.id}"
}

output "aws_subnet_private_1_id" {
  value = "${aws_subnet.private_1.id}"
}

output "aws_subnet_private_2_id" {
  value = "${aws_subnet.private_2.id}"
}

output "aws_subnet_public_1_id" {
  value = "${aws_subnet.public_1.id}"
}

output "aws_subnet_public_2_id" {
  value = "${aws_subnet.public_2.id}"
}

output "aws_vpc_main_id" {
  value = "${aws_vpc.main.id}"
}
