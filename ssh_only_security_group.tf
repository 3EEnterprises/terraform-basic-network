resource "aws_security_group" "ssh_only" {
  description = "ssh only"

  egress {
    cidr_blocks = ["0.0.0.0/0"]
    from_port = 0
    protocol = "-1"
    #security_groups = []
    #self = true
    to_port = 0
  }

  ingress {
    cidr_blocks = ["0.0.0.0/0"]
    from_port = 22
    protocol = "tcp"
    #security_groups = []
    #self = true
    to_port = 22
  }

  #name = ""
  name_prefix = "ssh_only_"

  tags {
    Name = "${var.project_name} (${var.instance}) / ssh_only"
  }

  vpc_id = "${aws_vpc.main.id}"
}
