resource "aws_security_group" "http_https_ssh" {
  description = "http https ssh"

  egress {
    cidr_blocks = ["0.0.0.0/0"]
    from_port = 0
    protocol = "-1"
    #security_groups = []
    #self = true
    to_port = 0
  }

  ingress {
    cidr_blocks = ["0.0.0.0/0"]
    from_port = 22
    protocol = "tcp"
    #security_groups = []
    #self = true
    to_port = 22
  }

  ingress {
    cidr_blocks = ["0.0.0.0/0"]
    from_port = 80
    protocol = "tcp"
    #security_groups = []
    #self = true
    to_port = 80
  }

  ingress {
    cidr_blocks = ["0.0.0.0/0"]
    from_port = 443
    protocol = "tcp"
    #security_groups = []
    #self = true
    to_port = 443
  }

  #name = ""
  name_prefix = "http_https_ssh_"

  tags {
    Name = "${var.project_name} (${var.instance}) / http_https_ssh"
  }

  vpc_id = "${aws_vpc.main.id}"
}
