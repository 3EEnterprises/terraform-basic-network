variable availability_zone_1 {}
variable availability_zone_2 {}

variable "cidr_block" {
  # must be between /16 and /28
  default = "10.0.0.0/16"
  description = "the cidr block for the vpc"
}

variable "instance" {}
variable "project_name" {}
